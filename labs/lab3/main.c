#include <stdio.h>
#include <progbase/console.h>

int main() {

    Console_clear();

    //Scene borders
    int LeftBord = 1;
    int RightBord = 80;
    int TopBord = 2;
    int BottomBord = 24;

    //Rectangle parametres
    int RectLeftBord = 38;
    int RectBottomBord = 15;
    int RectRightBord = 43;
    int RectTopBord = 9;

    //Needed parametres
    int x0;
    int y0;
    int key0 = 1;
    int step = 1;

    //Circle parametres
    int xc = 0;
    int yc = 0;
    int radius = 8;
    int centerX = 40;
    int centerY = 12;

    while(key0 != 10) {
        Console_reset();
        Console_clear();
        printf("WASD - circle controls, IJKL - rect controls, Enter - exit");

        //Set horizontal border colors
        Console_setCursorAttribute(BG_CYAN);
        Console_setCursorAttribute(FG_BLACK);

        //Set horizontal border parametres
        for(x0 = LeftBord; x0 <= RightBord; x0++) {
            y0 = TopBord;
            Console_setCursorPosition(y0, x0);
            printf("#\n");

            y0 = BottomBord;
            Console_setCursorPosition(y0, x0);
            printf("#\n");
        }

        //Set vertical border colors
        Console_setCursorAttribute(BG_CYAN);
        Console_setCursorAttribute(FG_BLACK);

        //Set vertical border parametres
        for(y0 = TopBord; y0 <= BottomBord; y0++) {
            x0 = 1;
            Console_setCursorPosition(y0, x0);
            printf("#\n");

            x0 = RightBord;
            Console_setCursorPosition(y0, x0);
            printf("#\n");
        }

        //Circle
        Console_setCursorAttribute(BG_BLUE);
        for (yc = -radius; yc <= radius; yc++) 
        {
            for (xc = -radius; xc <= radius; xc++)
            {
                if (xc * xc + yc * yc <= radius * radius)
                {
                Console_setCursorPosition(centerY + yc, centerX + xc);
                printf(" \n");
                }
            }
        }

        //Set rectangle parametres
        Console_setCursorAttribute(BG_INTENSITY_YELLOW);

        for(x0 = RectLeftBord; x0 < RectRightBord; x0++) {

            for(y0 = RectTopBord; y0 < RectBottomBord; y0++) {
                Console_setCursorPosition(y0, x0);
                printf(" \n");
            }
        }

        //Circle  & rectangle controls
        Console_reset();
        key0 = Console_getChar();

        switch(key0) {
            case 'w': centerY = centerY - step;
            break;
            case 'a': centerX = centerX  - step;
            break;
            case 's': centerY = centerY + step; 
            break;
            case 'd': centerX = centerX + step;
            break;
            case 'i': RectTopBord = RectTopBord - step;
            RectBottomBord = RectBottomBord - step;
            break;
            case 'j': RectLeftBord = RectLeftBord - step;
            RectRightBord = RectRightBord - step;
            break;
            case 'k': RectBottomBord = RectBottomBord + step;
            RectTopBord = RectTopBord + step;
            break;
            case 'l': RectRightBord = RectRightBord + step;
            RectLeftBord = RectLeftBord + step;
            break;
        }

        if(centerY == TopBord + radius) {
            ++centerY;
        }

        if(centerY == BottomBord - radius) {
            --centerY;
        }

        if(centerX == LeftBord + radius) {
            ++centerX;
        }

        if(centerX == RightBord - radius) {
            --centerX;
        }


        //Rectangle controls


        if(RectBottomBord == centerY + radius + 1) {
            --RectBottomBord;
            --RectTopBord;
        }

        if(RectLeftBord == centerX - radius + 1) {
            ++RectLeftBord;
            ++RectRightBord;
        }

        if(RectRightBord == centerX + radius) {
            --RectRightBord;
            --RectLeftBord;
        }

        if(RectTopBord == centerY - radius) {
            ++RectTopBord;
            ++RectBottomBord;
        }

    }

    Console_setCursorPosition(TopBord, RightBord);
    Console_reset();
    Console_clear();
    return 0;
}