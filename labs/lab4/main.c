#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <progbase.h>

int main()
{
    //design
    int xPosAr = 85;
    int yPosAr = 1;
    int xPosMen = 1;
    int yPosMen = 1;
    int xPosLine = 77;
    int yPosLine = 30;
    int yPosHorLine = 8;
    int xPosHorLine = 77;
    int align;
    int alignMax = 0;
    int alignMin = 0;
    int xMin = 0;
    int xMax = 0;
    int xPosSign = 33;
    int yPosSign = 9;
    int xPosProb = 34;
    int yPosProb = 23;
    int yPosOut = 16;
    int xPosOut = 35;

    //for array
    int N = 0;
    int b = 0;
    int keyMM = 0;
    int keyAr = 0;
    srand(time(0));
    float minVal;
    float maxVal;
    float multAr = 1.0;
    int indMax;
    int indMin;
    int redAr = 0;
    int L = 0;
    int H = 0;
    int belZer = 0;
    int qwert = belZer;
    bool flag;
    bool boolMinVal;
    bool boolMult;
    bool boolVal;
    bool boolMax;

    //for matrix
    int M = 0;
    int keyMat = 0;
    int i = 0;
    int j = 0;
    int maxValMat;
    int col = 0;
    int sumMat = 0;
    int minValMat = H;
    int Num = 0;
    int i1 = 0;
    int j1 = 0;
    int matIndMaxI = 0;
    int matIndMaxJ = 0;
    int matIndMinI = 0;
    int matIndMinJ = 0;
    bool boolMaxVal;
    bool boolCol;
    bool boolIns;
    bool boolAlign;

    //array menu options
    char arrMen1[] = "1. Заповнити масив випадковими числами від L до H";
    char arrMen2[] = "2. Обнулити всі елементи масиву";
    char arrMen3[] = "3. Знайти мінімальний елемент масиву та його індекс";
    char arrMen4[] = "4. Вивести добуток від'ємних елементів масиву";
    char arrMen5[] = "5. Поміняти місцями значення максимального і мінімального елементів масиву";
    char arrMen6[] = "6. Зменшити всі елементи масиву на введене число";
    char arrMen7[] = "7. Back";
    char arrMen32[] = "3. Знайти максимальний елемент та його індекси (i та j)";
    char arrMen42[] = "4. Знайти суму елементів стовпця за заданим індексом";
    char arrMen52[] = "5. Поміняти місцями максимальний і мінімальний елементи масиву";
    char arrMen62[] = "6. Змінити значення елементу за вказаними індексами на задане";

    int getColor(char colorCode);

    const char image[28][28] = {
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x7,0x7,0x7,0x7,0x7,0x2,0x7,0x7,0x7,0x7,0x7,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x7,0x7,0x7,0xF,0xF,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0x7,0x7,0x7,0xF,0xF,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0x7,0x7,0x7,0xF,0xF,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0x7,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0x7,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0x7,0x7,0x7,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0xD,0x7,0x7,0x7,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0x3,0x3,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0x3,0x3,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0x3,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD,0xD},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1},
    	{0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1,0x1}
    };

    do
    {
        Console_clear();
        keyAr = 0;
        maxValMat = 1;
        align = 0;
        boolCol = false;
        boolMaxVal = false;
        boolMinVal = false;
        boolMult = false;
        flag = false;
        boolIns = false;
        boolVal = false;
        boolMax = false;
        boolAlign = false;

        printf("1. Array\n2. Matrix\n3. Image\n4. Exit\n\n");

        printf("input: ");
        scanf("%i", &keyMM);

        if(keyMM == 1)
        {
            Console_clear();
            printf("Enter N: ");
            scanf("%i", &N);
        }

        float arr[N];

        if(keyMM == 1)
        {
            for(b = 0; b < N; b++)
            {
                arr[b] = 0.0;
            }
        }

        if(keyMM == 2)
        {
            Console_clear();

            printf("Enter N: ");
            scanf("%i", &N);
            printf("Enter M: ");
            scanf("%i", &M);
        }

        int mat[N][M];
                    
        if(keyMM == 2)
        {
            for(i = 0; i < N; i++)
            {
                for(j = 0; j < M; j++)
                {
                    mat[i][j] = 0;
                }
            }
        }

        while(keyMM == 1)
        {
            Console_clear();

            yPosAr = 1;

            //vertical line
            for(int yPosLine0 = 0; yPosLine0 <= yPosLine; yPosLine0++)
            {
                Console_setCursorPosition(yPosLine0, xPosLine);
                printf("|");
            }

            //USER INPUT SIGN
            Console_setCursorPosition(yPosSign, xPosSign);
            printf("USER INPUT");
            for(int xPosSign0 = 0; xPosSign0 < xPosSign; xPosSign0++)
            {
                Console_setCursorPosition(yPosSign, xPosSign0);
                printf(".");
            }
            for(int xPosSign0 = xPosSign + 10; xPosSign0 < xPosHorLine; xPosSign0++)
            {
                Console_setCursorPosition(yPosSign, xPosSign0);
                printf(".");
            }
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine, xPosHorLine0);
                printf(".");
            }

            //PROBLEMS SIGN
            Console_setCursorPosition(yPosProb, xPosProb);
            printf("PROBLEMS");
            for(int xPosProb0 = 0; xPosProb0 < xPosProb; xPosProb0++)
            {
                Console_setCursorPosition(yPosProb, xPosProb0);
                printf(".");
            }
            for(int xPosProb0 = xPosProb + 8; xPosProb0 < xPosHorLine; xPosProb0++)
            {
                Console_setCursorPosition(yPosProb, xPosProb0);
                printf(".");
            }
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine + 14, xPosHorLine0);
                printf(".");
            }

            //OUTPUT SIGN
            Console_setCursorPosition(yPosOut, xPosOut);
            printf("OUTPUT");
            for(int xPosOut0 = 0; xPosOut0 < xPosOut; xPosOut0++)
            {
                Console_setCursorPosition(yPosOut, xPosOut0);
                printf(".");
            }
            for(int xPosOut0 = xPosOut + 6; xPosOut0 < xPosHorLine; xPosOut0++)
            {
                Console_setCursorPosition(yPosOut, xPosOut0);
                printf(".");
            }
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine + 7, xPosHorLine0);
                printf(".");
            }

            //array
            for(b = 0; b < N; b++)
            {
                Console_setCursorAttribute(FG_BLACK);
                Console_setCursorAttribute(BG_WHITE);
                Console_setCursorPosition(yPosAr, xPosAr);
                printf("arr[%i] = %.1f\n", b, arr[b]);
                yPosAr += 2;
                Console_reset();
            }

            //min value
            if(boolMinVal == true)
            {
                Console_setCursorPosition(17, 1);
                printf("min value is arr[%i] = %.1f", indMin, minVal);
                boolMinVal = false;
            }

            //muliply
            if(flag == true && boolMult == true)
            {
                Console_setCursorPosition(17, 1);
                printf("answer: %.1f", multAr);
                flag = false;
                boolMult = false;
            }

            //no below zero multiply
            if(flag == false && boolMult == true)
            {
                Console_setCursorPosition(17, 1);
                printf("There are no numbers below zero");
                boolMult = false;
            }

            //boolVal
            if(boolVal == true)
            {
                Console_setCursorPosition(24, 1);
                printf("Error, min number is greater than max number\n");
                boolVal = false;
            }

            //main menu
            Console_setCursorPosition(yPosMen, xPosMen);
            printf("%s\n", arrMen1);
            printf("%s\n", arrMen2);
            printf("%s\n", arrMen3);
            printf("%s\n", arrMen4);
            printf("%s\n", arrMen5);
            printf("%s\n", arrMen6);
            printf("%s\n", arrMen7);

            Console_setCursorPosition(10, 1);
            printf("input: ");
            scanf("%i", &keyAr);

            switch(keyAr)
            {
                case 1:
                {
                    Console_setCursorPosition(13, 1);
                    printf("Enter min value: ");
                    scanf("%i", &L);
                    printf("Enter max value: ");
                    scanf("%i", &H);

                    if(L > H)
                    {
                        boolVal = true;
                    }

                    if(boolVal == false)
                    {
                        for(b = 0; b < N; b++)
                        {
                            arr[b] = rand() % (H - L + 1) + L;
                        }
                    }
                }
                break;

                case 2:
                {
                    for(b = 0; b < N; b++)
                    {
                        arr[b] = 0.0;
                    }
                }
                break;

                case 3:
                {
                    minVal = H;

                    for(b = 0; b < N; b++)
                    {
                        if(minVal > arr[b])
                        {
                            minVal = arr[b];
                            indMin = b;
                        }
                    }
                    boolMinVal = true;
                }
                break;

                case 4:
                {
                    multAr = 1;
                    qwert = 0;
                    belZer = 0;

                    for(b = 0; b < N; b++)
                    {
                        belZer = belZer + arr[b];

                        if(belZer < qwert)
                        {
                            flag = true;
                        }
                        
                        if(arr[b] < 0)
                        {
                            multAr = multAr * arr[b];
                        }

                        qwert = belZer;
                    }
                    boolMult = true;
                }
                break;

                case 5:
                {
                    minVal = H;
                    maxVal = L;

                    for(b = 0; b < N; b++)
                    {
                        if(arr[b] <= minVal)
                        {
                            minVal = arr[b];
                            indMin = b;
                        }

                        if(arr[b] >= maxVal)
                        {
                            maxVal = arr[b];
                            indMax = b;
                        }
                    }

                    arr[indMax] = minVal;
                    arr[indMin] = maxVal;
                }
                break;

                case 6:
                {
                    Console_setCursorPosition(14, 1);
                    printf("Enter number: ");
                    scanf("%i", &redAr);

                    for(b = 0; b < N; b++)
                    {
                        arr[b] = arr[b] - redAr;
                    }
                }
                break;

                case 7:
                {
                    keyMM = 0;
                }
                break;
            }
        }

        while(keyMM == 2) //matrix
        {
            Console_clear();

            yPosAr = 1;
            alignMin = 0;
            alignMax = 0;

            //vertical line
            for(int yPosLine0 = 0; yPosLine0 <= yPosLine; yPosLine0++)
            {
                Console_setCursorPosition(yPosLine0, xPosLine);
                printf("|");
            }  

            //horizontal line
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine, xPosHorLine0);
                printf("-");
            }

            //matrix
            for(i = 0; i < N; i++)
            {

                for(j = 0; j < M; j++)
                {
                    Console_setCursorAttribute(BG_WHITE);
                    Console_setCursorAttribute(FG_BLACK);
                    Console_setCursorPosition(yPosAr, xPosAr);
                    printf("%i", mat[i][j]);
                    xPosAr = xPosAr + align + 4;
                    Console_reset();
                }
                yPosAr += 2;
                xPosAr = 85;
            }

            //max value
            if(boolMaxVal == true)
            {
                Console_setCursorPosition(17, 1);
                printf("mat[%i][%i] = %i", matIndMaxI, matIndMaxJ, maxValMat);
                boolMaxVal = false;
            } 

            //sum of a column
            if(boolCol == true)
            {
               Console_setCursorPosition(17, 1); 
               printf("sum is %i", sumMat);
               boolCol = false;
            }

            //inserted i and j
            if(boolIns == true)
            {
                Console_setCursorPosition(24, 1);
                printf("Error, inserted 'i' or 'j' is greater than possible\n");
                boolIns = false;
            }

            //USER INPUT SIGN
            Console_setCursorPosition(yPosSign, xPosSign);
            printf("USER INPUT");
            for(int xPosSign0 = 0; xPosSign0 < xPosSign; xPosSign0++)
            {
                Console_setCursorPosition(yPosSign, xPosSign0);
                printf(".");
            }
            for(int xPosSign0 = xPosSign + 10; xPosSign0 < xPosHorLine; xPosSign0++)
            {
                Console_setCursorPosition(yPosSign, xPosSign0);
                printf(".");
            }
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine, xPosHorLine0);
                printf(".");
            }

            //PROBLEMS SIGN
            Console_setCursorPosition(yPosProb, xPosProb);
            printf("PROBLEMS");
            for(int xPosProb0 = 0; xPosProb0 < xPosProb; xPosProb0++)
            {
                Console_setCursorPosition(yPosProb, xPosProb0);
                printf(".");
            }
            for(int xPosProb0 = xPosProb + 8; xPosProb0 < xPosHorLine; xPosProb0++)
            {
                Console_setCursorPosition(yPosProb, xPosProb0);
                printf(".");
            }
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine + 14, xPosHorLine0);
                printf(".");
            }

            //OUTPUT SIGN
            Console_setCursorPosition(yPosOut, xPosOut);
            printf("OUTPUT");
            for(int xPosOut0 = 0; xPosOut0 < xPosOut; xPosOut0++)
            {
                Console_setCursorPosition(yPosOut, xPosOut0);
                printf(".");
            }
            for(int xPosOut0 = xPosOut + 6; xPosOut0 < xPosHorLine; xPosOut0++)
            {
                Console_setCursorPosition(yPosOut, xPosOut0);
                printf(".");
            }
            for(int xPosHorLine0 = 0; xPosHorLine0 < xPosHorLine; xPosHorLine0++)
            {
                Console_setCursorPosition(yPosHorLine + 7, xPosHorLine0);
                printf(".");
            }

            //boolVal
            if(boolVal == true)
            {
                Console_setCursorPosition(24, 1);
                printf("Error, min number is greater than max number");
                boolVal = false;
            }

            //boolMax
            if(boolMax == true)
            {
                Console_setCursorPosition(24, 1);
                printf("Error, inserted 'i' is greater or lower than possible\n");
                boolMax = false;
            }

            Console_setCursorPosition(yPosMen, xPosMen);   
            printf("%s\n", arrMen1);
            printf("%s\n", arrMen2);
            printf("%s\n", arrMen32);
            printf("%s\n", arrMen42);
            printf("%s\n", arrMen52);
            printf("%s\n", arrMen62);
            printf("%s\n", arrMen7); 

            Console_setCursorPosition(10, 1);
            printf("input: ");
            scanf("%i", &keyMat);

            switch(keyMat)
            {
                case 1:
                {
                    Console_setCursorPosition(13, 1);
                    printf("Min val is: ");
                    scanf("%i", &L);
                    printf("Max val is: ");
                    scanf("%i", &H);

                    maxValMat = L;
                    minValMat = H;

                    if(L > H)
                    {
                        boolVal = true;
                    }

                    if(boolVal == false)
                    {
                        for(i = 0; i < N; i++)
                        {
                            for(j = 0; j < M; j++)
                            {
                                mat[i][j] = rand() % (H - L + 1) + L;

                                if(maxValMat < mat[i][j])
                                {
                                    maxValMat = mat[i][j];
                                    matIndMaxI = i;
                                    matIndMaxJ = j;
                                }

                                if(minValMat > mat[i][j])
                                {
                                    minValMat = mat[i][j];
                                    matIndMinI = i;
                                    matIndMinJ = j;
                                }
                            }
                        }

                        xMax = maxValMat;
                        xMin = minValMat;

                        //align
                        if(xMax > 0)
                        {
                            do
                            {
                                xMax = xMax / 10;  
                                alignMax += 1;

                            }while(xMax > 0);
                        }

                        if(xMin < 0)
                        {
                            do
                            {
                                xMin = xMin / 10;
                                alignMin += 1;

                            }while(xMin < 0);
                        }

                        if(alignMin > alignMax)
                        align = alignMin;

                        if(alignMin <= alignMax)
                        align = alignMax;

                        if(align >= 1)
                        align = align - 1;
                    }
                }
                break;

                case 2:
                {
                    for(i = 0; i < N; i++)
                    {
                        for(j = 0; j < M; j++)
                        {
                            mat[i][j] = 0;
                        }
                    }

                    align = 0;
                    maxValMat = 0;
                    matIndMaxI = 0;
                    matIndMaxJ = 0;
                    L = 0;
                    H = 0;
                }
                break;

                case 3:
                {
                    maxValMat = L;
                    boolMaxVal = true;

                    for(i = 0; i < N; i++)
                    {
                        for(j = 0; j < M; j++)
                        {
                            if(maxValMat < mat[i][j])
                            {
                                maxValMat = mat[i][j];
                                matIndMaxI = i;
                                matIndMaxJ = j;
                            }
                        }
                    }
                }
                break;

                case 4:
                {
                    Console_setCursorPosition(14, 1);
                    printf("Enter i: ");
                    scanf("%i", &col);
                    sumMat = 0;

                    if(col < 0 || col >= N)
                    {
                        boolMax = true;
                    }

                    if(boolMax == false)
                    {
                        for(i = 0; i < N; i++)
                        {
                            for(j = 0; j < M; j++)
                            {
                                if(col == j)
                                {
                                    sumMat = sumMat + mat[i][j];
                                }
                            }
                        }

                        boolCol = true;
                    }
                }
                break;

                case 5:
                {      
                    maxValMat = L;
                    minValMat = H;   

                    for(i = 0; i < N; i++)
                    {
                        for(j = 0; j < M; j++)
                        {
                            if(maxValMat <= mat[i][j])
                            {
                                maxValMat = mat[i][j];
                                matIndMaxI = i;
                                matIndMaxJ = j;
                            }

                            if(minValMat >= mat[i][j])
                            {
                                minValMat = mat[i][j];
                                matIndMinI = i;
                                matIndMinJ = j;
                            }
                        }
                    }
                    mat[matIndMaxI][matIndMaxJ] = minValMat;
                    mat[matIndMinI][matIndMinJ] = maxValMat;
                }
                break;

                case 6:
                {
                    Console_setCursorPosition(12, 1);
                    printf("Enter i: ");
                    scanf("%i", &i1);
                    printf("Enter j: ");
                    scanf("%i", &j1);
                    printf("Enter new value: ");
                    scanf("%i", &Num);

                    if(i1 >= N || j1 >= M)
                    {
                        boolIns = true;
                    }

                    if(boolIns == false)
                    {
                        mat[i1][j1] = Num;

                        for(i = 0; i < N; i++)
                        {
                            for(j = 0; j < M; j++)
                            {
                                if(mat[i][j] > maxValMat)
                                {
                                    maxValMat = mat[i][j];
                                }

                                if(mat[i][j] < minValMat)
                                {
                                    minValMat = mat[i][j];
                                }
                            }
                        }

                        xMax = maxValMat;
                        xMin = minValMat;

                        //align
                        if(xMax > 0)
                        {
                            do
                            {
                                xMax = xMax / 10;  
                                alignMax += 1;

                            }while(xMax > 0);
                        }

                        if(xMin < 0)
                        {
                            do
                            {
                                xMin = xMin / 10;
                                alignMin += 1;

                            }while(xMin < 0);
                        }

                        if(alignMin > alignMax)
                        align = alignMin;

                        if(alignMin <= alignMax)
                        align = alignMax;

                        if(align == 1)
                        align = 0;
                    }
                }
                break;

                case 7:
                {
                    keyMM = 0;
                }
                break;
            }
        
        }

        if(keyMM == 3)
        {
            Console_clear();

            for(int k = 0; k < 28; k++)
            {
                for(int l = 0; l < 28; l++)
                {
                    int color = getColor(image[k][l]); 
                    Console_setCursorAttribute(color);
                    printf("  "); 
                    Console_reset();
                }
                printf("\n");
            }
            printf("Press any key to return\n");
            char a;
            a = Console_getChar();
            keyMM = 0;
        }
        
    }while(keyMM != 4);

    Console_reset();
    Console_clear();
    return 0;
}

int getColor(char colorCode) {

    const char colorsTable[16][2] = {
        {0x0, BG_BLACK},
        {0x1, BG_INTENSITY_BLACK},
        {0x2, BG_RED},
        {0x3, BG_INTENSITY_RED},
        {0x4, BG_GREEN},
        {0x5, BG_INTENSITY_GREEN},
        {0x6, BG_YELLOW},
        {0x7, BG_INTENSITY_YELLOW},
        {0x8, BG_BLUE},
        {0x9, BG_INTENSITY_BLUE},
        {0xa, BG_MAGENTA},
        {0xb, BG_INTENSITY_MAGENTA},
        {0xc, BG_CYAN},
        {0xd, BG_INTENSITY_CYAN},
        {0xe, BG_WHITE},
        {0xf, BG_INTENSITY_WHITE}
    };
    const int tableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    for (int i = 0; i < tableLength; i++) 
    {
        char colorPairCode = colorsTable[i][0];
        char colorPairColor = colorsTable[i][1];
        if (colorCode == colorPairCode) 
        {
            return colorPairColor; 
        }
    }
    Console_reset();
    Console_clear();
    
    return 0;
}