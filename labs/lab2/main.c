#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main()
{
    const float xmin = -10.0;
    const float xmax = 10.0;
    const float xstep = 0.5;
    float x = xmin;
    float y = 0;

    while (x <= xmax)
    {
   
        if (x == 0)
        {
            printf("ERROR\n");

        }
        else if  ((x >= -4.5 && x < -1) || (x > 1 && x <=4.5))
        {
            y = (sin(pow(x, 2))) + (pow(cos(x), 2) / x);
            printf("%f\n", y);
        } 
        else if (x < -4.5 || (x >= -1 && x <= 1) || x > 4.5)
        {
            y = 1 / (3 * x);
            printf("%f\n", y);
        }
         x += xstep;
    }
    return 0;
}