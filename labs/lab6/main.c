#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <progbase.h>
#include <progbase/canvas.h>
#include <progbase/console.h>
#include <math.h>

struct Colors
{
    int red;
    int green;
    int blue;
};

struct Vec2D
{
    int x;
    int y;
};

struct Ball
{
    struct Vec2D loc;
    int radius;
    float rotRadius;
    float speed;
};

int main()
{
    Console_clear();

    Canvas_invertYOrientation();
    struct ConsoleSize consoleSize = Console_size();
    const int widthPixels = consoleSize.columns;
    const int heightPixels = consoleSize.rows * 2;
    float cx0 = widthPixels / 2;
    float cy0 = heightPixels / 2;
    Canvas_setSize(widthPixels, heightPixels);

    srand(time(0));

    const float pi = 3.1415;
    const float dt = 0.0001;
    const int numBalls = 7;
    float rotAngle = 0;
    int oldTime = time(0);
    int newTime;
    int tiime;
    int key = 1;
    int minus = 7;
    int rotRadMax = 24;
    int N1 = 6;
    int N2 = 2;
    int R = 7;

    struct Colors col[numBalls];
    struct Ball ball[numBalls];

    for (int i = 0; i < numBalls; i++)
    {
        col[i].red = rand() % 256;
        col[i].green = rand() % 256;
        col[i].blue = rand() % 256;
        ball[i].rotRadius = rotRadMax - minus;
        minus += 1;
        ball[i].speed = (rand() % (N1 - N2 + 1) + N2) * dt;
        key = rand() % (2 - 1 + 1) + 1;

        if (key == 2)
        {
            ball[i].speed = -ball[i].speed;
        }

        ball[i].radius = R;
        R -= 1;
    }

    while (!Console_isKeyDown())
    {
        newTime = time(0);
        rotAngle += pi / 2;

        ball[0].loc.x = cx0;
        ball[0].loc.y = cy0;

        for (int i = 1; i < numBalls; i++)
        {
            ball[i].loc.x = ball[i - 1].loc.x + ball[i].rotRadius * cos(rotAngle * ball[i].speed);
            ball[i].loc.y = ball[i - 1].loc.y + ball[i].rotRadius * sin(rotAngle * ball[i].speed);
        }

        Canvas_beginDraw();
        //
        for (int i = 0; i < numBalls; i++)
        {
            Canvas_setColorRGB(col[i].red, col[i].green, col[i].blue);
            Canvas_fillCircle(ball[i].loc.x, ball[i].loc.y, ball[i].radius);
        }
        //
        Canvas_endDraw();
    }

    Console_clear();
    return 0;
}