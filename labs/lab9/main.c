#include <stdlib.h>
#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

enum TOKENS
{
    TOKEN_KEYWORD,
    TOKEN_OPERATOR,
    TOKEN_DELIMITER,
    TOKEN_LITERAL,
    TOKEN_IDENTIFIER,
    TOKEN_WHITESPACE,
};

enum LIT
{
    LIT_INTEGER,
    LIT_FLOAT,
    LIT_STRING,
};

enum OP
{
    OP_ASSIGNMENT,
    OP_ADD,
    OP_SUBSTRACT,
    OP_MULTIPLY,
};

enum KW
{
    KW_INTEGER,
    KW_RETURN,
};

enum DEL
{
    DEL_COMMA,
    DEL_SEMICOLON,
    DEL_LEFTPAR,
    DEL_RIGHTPAR,
    DEL_LEFTCURL,
    DEL_RIGHTCURL,
};

struct Enums
{
    enum LIT lit;
    enum OP op;
    enum KW kw;
    enum DEL del;
};

struct TextPosition
{
    int row;
    int column;
};

struct Table
{
    char *items;
    size_t row_capacity;
    size_t col_capacity;
    size_t count;
};

struct token
{
    int count_tok;
    char *token_pointer;
    enum TOKENS tokType;
    struct Enums enums;
    struct TextPosition textPos;
};

void addString(struct Table *ptable, char *str);
int parsecode(char *p, struct Table *ptable, struct token *ptok, struct Table *plit, struct Table *pid, int *pNumTok, int *pNumId, int *pNumLit, int *pNumKw, struct Table *whitesp);
int standart(char *p, struct Table *ptable, struct token *ptok, struct Table *plit, struct Table *pid, int *pNumTok, struct Table *whitesp);
char *readWordMod(char *dest, char *src, int length);
char *readNum(char *dest, char *src, int length, int *pInt, int *pcolNum);
char *readPunkt(char *dest, char *src);
char *readWord(char *dest, char *src, int length, bool *isLit, int *pcolNum);
bool containString(struct Table *ptable, char *str);
char *readString(struct Table *ptable, int i);
void printappopt(struct token *t, int *pNumTok);
char *findString(const struct Table *ptable, const char *str);
char *getStringAt(const struct Table *ptable, int index);

int main(int argc, char *argv[])
{
    Console_clear();

    const int nKeywords = 50;
    const int nMaxKwLength = 50;
    const int nMaxIdentifiers = 50;
    const int nMaxIdLength = 50;
    const int nMaxLiterals = 50;
    const int nMaxLitLength = 50;
    const int nMaxWhiteSp = 200;
    const int nMaxWhiteSpLen = 10;

    char kwords[nKeywords][nMaxKwLength];
    char id[nMaxIdentifiers][nMaxIdLength];
    char lit[nMaxLiterals][nMaxLitLength];
    char whitesp[nMaxWhiteSp][nMaxWhiteSpLen];

    struct Table keywords;
    struct Table identifiers;
    struct Table literals;
    struct Table whitespaces;
    struct token token_st[100];

    keywords.count = 0;
    keywords.row_capacity = nMaxKwLength;
    keywords.col_capacity = nKeywords;
    keywords.items = &kwords[0][0];

    identifiers.count = 0;
    identifiers.row_capacity = nMaxIdentifiers;
    identifiers.col_capacity = nMaxIdLength;
    identifiers.items = &id[0][0];

    literals.count = 0;
    literals.row_capacity = nMaxLiterals;
    literals.col_capacity = nMaxLitLength;
    literals.items = &lit[0][0];

    whitespaces.count = 0;
    whitespaces.row_capacity = nMaxWhiteSp;
    whitespaces.col_capacity = nMaxWhiteSpLen;
    whitespaces.items = &whitesp[0][0];

    addString(&keywords, "int");
    addString(&keywords, "return");

    char read;
    char str[500];
    int i = 0;
    int ii = 0;
    char strr[500];

    bool isInp = false;
    bool isOut = false;
    bool isl = false;

    int numTokens = 0;
    int *pNumTok = &numTokens;

    int numLit = 0;
    int *pNumLit = &numLit;

    int numId = 0;
    int *pNumId = &numId;

    int numKw = 0;
    int *pNumKw = &numKw;

    FILE *fp;

    if (argc == 1)
    {
        if ((fp = fopen("input.txt", "r")) == NULL)
        {
            printf("Can not open input.txt\n");
            exit(EXIT_FAILURE);
        }

        read = fgetc(fp);
        i = 0;

        while (read != EOF)
        {
            str[i++] = read;
            read = fgetc(fp);
        }
        str[i] = '\0';

        standart(str, &keywords, token_st, &literals, &identifiers, pNumTok, &whitespaces);
        fclose(fp);

        printf("\n");
        printappopt(token_st, pNumTok);
        printf("\n");

        return 0;
    }

    for (int i = 1; i < argc; i++)
    {
        if (isOut == true && isalpha(*argv[i]))
        {

            fp = freopen(argv[i], "w", stdout);
            if (fp == NULL)
            {
                printf("Can not open file %s\n", argv[i]);
                exit(EXIT_FAILURE);
            }

            printf("%s", str);
            fclose(fp);
        }
        if (!strcmp(argv[i], "-o"))
        {
            if (isInp == false)
            {
                printf("Input file is missing\n");
                return 0;
            }

            if ((i + 1) == argc || *argv[i + 1] == '-')
            {
                printf("Output file is missing\n");
                return 0;
            }
            isOut = true;
        }
        else if (isalpha(*argv[i]) && isInp == false)
        {

            if ((fp = fopen(argv[i], "r")) == NULL)
            {
                printf("Can not open input file\n");
                return 0;
            }

            ii = 0;
            read = fgetc(fp);
            while (read != EOF)
            {
                str[ii++] = read;
                read = fgetc(fp);
            }
            str[ii] = '\0';

            fclose(fp);

            isInp = true;
        }
        else if (!strcmp(argv[i], "-l"))
        {
            isl = true;
        }
    }

    if (isl == false && isOut == false && isInp == true)
    {
        printf("Option is missing\n");
    }
    if (isl == false && isOut == true && isInp == true)
    {
        fp = fopen(argv[i], "r");
        ii = 0;
        while (read != EOF)
        {
            str[ii++] = read;
            read = fgetc(fp);
        }
        str[ii] = '\0';

        printf("%s", strr);
        fclose(fp);
    }

    if (isInp == false && isl == true)
    {
        printf("Input file is missing\n");
    }

    if (isInp == true && isl == true && isOut == false)
    {
        if (parsecode(str, &keywords, token_st, &literals, &identifiers, pNumTok, pNumId, pNumLit, pNumKw, &whitespaces) == 1)
        {
            return 1;
        }

        printf(">>>\n\n");
        printf("%s\n", str);
        printf("\n<<<\n\n");

        printf("\n");
        printappopt(token_st, pNumTok);
        printf("\n");

        printf("\nAmount of tokens: %d\n", *pNumTok);

        int a = 0;

        printf("\nLITERALS (%i):\n", numLit);
        while (a < numLit)
        {
            printf("%s\n", &lit[a][0]);
            a++;
        }

        a = 0;

        printf("\nIDENTIFIERS (%i):\n", numId);
        while (a < numId)
        {
            printf("%s\n", &id[a][0]);
            a++;
        }

        a = 0;

        printf("\nKEYWORDS (%i):\n", numKw);
        while (a < numKw)
        {
            printf("%s\n", &kwords[a][0]);
            a++;
        }
    }

    return 0;
}

int parsecode(char *p, struct Table *ptable, struct token *ptok, struct Table *plit, struct Table *pid, int *pNumTok, int *pNumId, int *pNumLit, int *pNumKw, struct Table *whitesp)
{
    const int bufLen = 50;
    char buf[bufLen];
    int *pInt;
    int number = 0;
    pInt = &number;
    bool intRead = false;
    bool retRead = false;
    bool isLit = false;

    char rowNum = 0;

    int colNum = 0;
    int *pcolNum = &colNum;

    while (*p)
    {
        if (*p == '\n')
        {
            rowNum += 1;
            *pcolNum = -1;
            whitesp->count -= 1;
        }

        if (isspace(*p))
        {
            whitesp->count += 1;
            if (containString(whitesp, " ") == false)
            {
                addString(whitesp, " ");
            }
            p++;
            *pcolNum += 1;
        }

        if (ispunct(*p))
        {
            ptok->textPos.column = *pcolNum;

            *pcolNum += 1;

            readPunkt(buf, p);

            if (strcmp(buf, "\"") == 0)
            {
                p++;
                if (isalpha(*p))
                {
                    *pcolNum += 2;

                    isLit = true;
                    p = readWord(buf, p, bufLen, &isLit, pcolNum);

                    if (containString(plit, buf) == false)
                    {
                        addString(plit, buf);
                        *pNumLit += 1;
                    }

                    ptok->token_pointer = findString(plit, buf);
                    ptok->enums.lit = LIT_STRING;
                    ptok->tokType = TOKEN_LITERAL;
                    ptok->count_tok += 1;
                    *pNumTok += 1;
                    ptok->textPos.row = rowNum;
                    p++;
                    ptok++;
                    continue;
                }
                else
                {
                    p--;
                }
            }

            if (strcmp(buf, "\"") == 0 || strcmp(buf, "\\") == 0)
            {
                p++;
                *pcolNum -= 1;
                continue;
            }

            if (*p != '-' && *p != '(' && *p != ')' && *p != '}' && *p != '{' && *p != '_' && *p != '+')
            {
                p++;
                if (isalpha(*p))
                {
                    Console_clear();
                    printf("ERROR: WRONG CHAR\n");
                    return 1;
                }

                else
                {
                    p--;
                }
            }

            if (*p == '.')
            {
                p++;

                if (isdigit(*p))
                {
                    Console_clear();
                    printf("ERROR: WRONG FLOAT\n");
                    return 1;
                }
                else
                    p -= 1;
            }

            if (strcmp(buf, ";") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_SEMICOLON;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, ",") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_SEMICOLON;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "{") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_LEFTCURL;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "}") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_RIGHTCURL;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "(") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_LEFTPAR;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, ")") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_RIGHTPAR;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "-") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_SUBSTRACT;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "+") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_ADD;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "=") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_ASSIGNMENT;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            if (strcmp(buf, "*") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_MULTIPLY;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok->textPos.row = rowNum;
                ptok->textPos.column = *pcolNum - 1;
                ptok++;
            }

            p++;
            continue;
        }

        if (isalpha(*p) || *p == '_')
        {
            ptok->textPos.column = *pcolNum;

            p = readWord(buf, p, bufLen, &isLit, pcolNum);

            if (strcmp(buf, "n") == 0 || strcmp(buf, "t") == 0)
            {
                p++;
                *pcolNum += 1;
                continue;
            }

            if (containString(ptable, buf) == true)
            {
                for (int j = 0; j < ptable->count; j++)
                {

                    if (strcmp(buf, "int") == 0)
                    {
                        ptok->token_pointer = findString(ptable, buf);
                        ptok->tokType = TOKEN_KEYWORD;
                        ptok->enums.kw = KW_INTEGER;
                        ptok->count_tok += 1;
                        *pNumTok += 1;
                        ptok->textPos.row = rowNum;

                        if (intRead == false)
                        {
                            *pNumKw += 1;
                            intRead = true;
                        }

                        ptok++;
                        *pcolNum += 1;
                        break;
                    }

                    if (strcmp(buf, "return") == 0)
                    {
                        ptok->token_pointer = findString(ptable, buf);
                        ptok->tokType = TOKEN_KEYWORD;
                        ptok->enums.kw = KW_RETURN;
                        ptok->count_tok += 1;
                        *pNumTok += 1;
                        ptok->textPos.row = rowNum;

                        if (retRead == false)
                        {
                            *pNumKw += 1;
                            retRead = true;
                        }
                        ptok++;
                        *pcolNum += 1;
                        break;
                    }
                }
            }

            else
            {
                if (containString(plit, buf) == true)
                {
                    p++;
                    continue;
                }
                else
                {
                    if (containString(pid, buf) == false)
                    {
                        addString(pid, buf);
                        *pNumId += 1;
                    }

                    ptok->tokType = TOKEN_IDENTIFIER;
                    ptok->token_pointer = findString(pid, buf);
                    ptok->count_tok += 1;
                    *pNumTok += 1;
                    ptok->textPos.row = rowNum;
                    ptok += 1;
                    p++;
                    continue;
                }
            }
        }

        if (isalnum(*p))
        {
            ptok->textPos.column = *pcolNum;

            p = readNum(buf, p, bufLen, pInt, pcolNum);

            if (*p == ';')
                p--;

            if (strcmp(buf, "") == 0)
            {
                p++;
                continue;
            }

            if (*pInt == 0)
            {
                ptok->enums.lit = LIT_INTEGER;
                ptok->textPos.row = rowNum;
            }

            if (*pInt == 1)
            {
                ptok->enums.lit = LIT_FLOAT;
                *pInt = 0;
                ptok->textPos.row = rowNum;
            }

            if (containString(plit, buf) == false)
            {
                addString(plit, buf);
                *pNumLit += 1;
            }

            ptok->tokType = TOKEN_LITERAL;
            ptok->token_pointer = findString(plit, buf);
            ptok->count_tok += 1;
            *pNumTok += 1;
            ptok += 1;
            p++;
            *pcolNum += 1;

            continue;
        }
        p++;
    }

    return 0;
}

int standart(char *p, struct Table *ptable, struct token *ptok, struct Table *plit, struct Table *pid, int *pNumTok, struct Table *whitesp)
{
    const int bufLen = 50;
    char buf[bufLen];
    int *pInt;
    int number = 0;
    pInt = &number;
    int n = 0;
    int *pcolNum = &n;
    bool isList = false;

    while (*p)
    {
        if (isspace(*p))
        {
            whitesp->count += 1;
            addString(whitesp, buf);
            p--;
            if (*p != ';')
                printf(" ");

            p += 2;
        }

        if (*p == '\n')
        {
            printf("\n");
        }

        if (ispunct(*p))
        {
            readPunkt(buf, p);

            if (*p == '"')
            {
                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                p++;

                if (*p == '\\')
                {
                    p++;

                    if (*p == 't')
                    {
                        Console_setCursorAttribute(FG_YELLOW);
                        printf("\\t");
                        p++;
                        continue;
                    }
                    if (*p == 'n')
                    {
                        Console_setCursorAttribute(FG_YELLOW);
                        printf("\\n");
                        p++;
                        continue;
                    }
                    else
                    {
                        printf("\\");
                    }
                    //continue;
                }

                if (isalpha(*p) && *p != 't' && *p != 'n')
                {
                    p = readWordMod(buf, p, bufLen);

                    if (containString(plit, buf) == false)
                    {
                        addString(plit, buf);
                    }

                    Console_setCursorAttribute(FG_BLUE);
                    printf("%s", buf);
                    Console_setCursorAttribute(FG_RED);
                    continue;
                }
                else
                {
                    Console_setCursorAttribute(FG_BLUE);
                    continue;
                }
            }

            if (strcmp(buf, "\\") == 0)
            {
                p++;

                if (*p == 't')
                {
                    Console_setCursorAttribute(FG_YELLOW);
                    printf("\\t");
                    p++;
                    continue;
                }
                if (*p == 'n')
                {
                    Console_setCursorAttribute(FG_YELLOW);
                    printf("\\n");
                    p++;
                    continue;
                }

                // if (*p == 'n' || *p == 't')
                // {
                //     Console_setCursorAttribute(FG_YELLOW);
                //     printf("%s", buf);
                // }
                else
                {
                    printf("%s", buf);
                    continue;
                }
            }

            if (*p != '-' && *p != '(' && *p != ')' && *p != '}' && *p != '{' && *p != '_')
            {
                p++;
                if (isalpha(*p))
                {
                    Console_clear();
                    printf("ERROR: WRONG CHAR\n");
                    return 1;
                }

                else
                {
                    p--;
                }
            }

            if (*p == '.')
            {
                p++;

                if (isdigit(*p))
                {
                    Console_clear();
                    printf("ERROR: WRONG FLOAT\n");
                    return 1;
                }
                else
                    p--;
            }

            if (strcmp(buf, ";") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                p++;
                if (*p == '\n')
                {
                    printf("\n");
                }
                continue;
            }

            if (strcmp(buf, ",") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
            }

            if (strcmp(buf, "{") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
            }

            if (strcmp(buf, "}") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
            }

            if (strcmp(buf, "(") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
                p++;
                continue;
            }

            if (strcmp(buf, ")") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
                p++;
                continue;
            }

            if (strcmp(buf, "-") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
                p++;
                continue;
            }

            if (strcmp(buf, "+") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
                p++;
                continue;
            }

            if (strcmp(buf, "*") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
                p++;
                continue;
            }

            if (strcmp(buf, "=") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                }

                Console_setCursorAttribute(FG_RED);
                printf("%s", buf);
                ptok++;
                p++;
                continue;
            }

            printf("%s", buf);
        }

        if (isalpha(*p) || *p == '_')
        {
            p = readWord(buf, p, bufLen, &isList, pcolNum);

            if (strcmp(buf, "int") == 0)
            {
                Console_setCursorAttribute(FG_GREEN);
                printf("%s", buf);
                p++;
                continue;
            }

            if (strcmp(buf, "return") == 0)
            {
                Console_setCursorAttribute(FG_GREEN);
                printf("%s", buf);
                p++;
                continue;
            }

            else
            {
                if (containString(plit, buf) == true)
                {
                    p++;
                    continue;
                }
                else
                {
                    if (containString(pid, buf) == false)
                    {
                        addString(pid, buf);
                    }

                    Console_setCursorAttribute(FG_CYAN);
                    printf("%s", buf);
                }
            }
        }

        if (isalnum(*p))
        {
            p = readNum(buf, p, bufLen, pInt, pcolNum);

            if (*p == ';')
                p--;

            Console_setCursorAttribute(FG_BLUE);
            printf("%s", buf);
        }
        p++;
    }
    printf("\n");
    return 0;
}

char *getStringAt(
    const struct Table *ptable,
    int index)
{
    char *p = (*ptable).items;
    p += index * (*ptable).row_capacity;
    return p;
}

char *findString(
    const struct Table *ptable,
    const char *str)
{
    for (int i = 0; i < (*ptable).count; i++)
    {
        char *p = getStringAt(ptable, i);
        if (strcmp(p, str) == 0)
        {
            return p;
        }
    }
    return NULL;
}

void addString(struct Table *ptable, char *str)
{
    int row_index = (*ptable).count;
    char *p = (*ptable).items;
    p += row_index * (*ptable).row_capacity;
    strcpy(p, str);
    (*ptable).count += 1;
}

char *readString(struct Table *ptable, int i)
{
    char *p = (*ptable).items;
    p += i * (*ptable).row_capacity;

    return p;
}

bool containString(struct Table *ptable, char *str)
{
    int length = (*ptable).count;
    for (int i = 0; i < length; i++)
    {
        char *p = readString(ptable, i);
        if (strcmp(p, str) == 0)
        {
            return true;
        }
    }

    return false;
}

char *readWord(char *dest, char *src, int length, bool *isLit, int *pcolNum)
{
    int counter = 0;
    if (*isLit == true)
    {
        *dest = '\\';
        dest++;
        *dest = '\"';
        dest++;

        while (isalnum(*src) || *src == '_' || *src == '"' || *src == '\\')
        {
            if (++counter >= length)
            {
                return (NULL);
            }
            *pcolNum += 1;
            *dest = *src;
            dest++;
            src++;
        }
        dest--;
    }
    else
    {
        while (isalnum(*src) || *src == '_')
        {
            if (++counter >= length)
            {
                return (NULL);
            }
            *pcolNum += 1;
            *dest = *src;
            dest++;
            src++;
        }
    }
    *dest = '\0';
    src--;
    *isLit = false;
    return src;
}

char *readWordMod(char *dest, char *src, int length)
{
    int counter = 0;

    while (isalnum(*src) || *src == '_')
    {
        if (++counter >= length)
        {
            return (NULL);
        }

        *dest = *src;
        dest++;
        src++;
    }

    *dest = '\0';
    return src;
}

char *readPunkt(char *dest, char *src)
{
    *dest = *src;
    src++;
    dest++;
    *dest = '\0';

    return src;
}

char *readNum(char *dest, char *src, int length, int *pInt, int *pcolNum)
{
    int counter = 0;
    *pInt = 0;

    while (isdigit(*src))
    {
        *dest = *src;
        dest++;
        *pcolNum += 1;
        if (++counter >= length)
        {
            return (NULL);
        }
        src++;
    }

    if (*src == '.')
    {
        *pInt = 1;
        *dest = *src;
        dest++;
        src++;
        *pcolNum += 1;
        if (!isdigit(*src))
        {
            Console_clear();
            printf("ERROR: WRONG FLOAT\n");
            return (NULL);
        }
        else
        {
            while (isdigit(*src))
            {
                *pcolNum += 1;
                *dest = *src;
                dest++;
                src++;
            }
            src--;
        }
    }
    *pcolNum -= 1;
    *dest = '\0';

    return src;
}

void printappopt(struct token *t, int *pNumTok)
{
    for (int i = 0; i < *pNumTok; i++)
    {
        switch (t[i].tokType)
        {
        case TOKEN_WHITESPACE:
            break;

        case TOKEN_KEYWORD:
            printf("TOKEN_KEYWORD");

            switch (t[i].enums.kw)
            {
            case KW_INTEGER:
                printf("\tKW_INTEGER");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case KW_RETURN:
                printf("\tKW_RETURN");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;
            }
            break;

        case TOKEN_LITERAL:
            printf("TOKEN_LITERAL");

            switch (t[i].enums.lit)
            {
            case LIT_FLOAT:
                printf("\tLIT_FLOAT");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case LIT_INTEGER:
                printf("\tLIT_INTEGER");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case LIT_STRING:
                printf("\tLIT_STRING");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;
            }
            break;

        case TOKEN_DELIMITER:
            printf("TOKEN_DELIMITER");

            switch (t[i].enums.del)
            {
            case DEL_COMMA:
                printf("\tDEL_COMMA");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case DEL_LEFTCURL:
                printf("\tDEL_LEFTCURL");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case DEL_LEFTPAR:
                printf("\tDEL_LEFTPAR");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case DEL_RIGHTCURL:
                printf("\tDEL_RIGHTCURL");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case DEL_RIGHTPAR:
                printf("\tDEL_RIGHTPAR");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case DEL_SEMICOLON:
                printf("\tDEL_SEMICOLON");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;
            }
            break;

        case TOKEN_IDENTIFIER:
            printf("TOKEN_IDENTIFIER");

            printf("\t\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
            break;

        case TOKEN_OPERATOR:
            printf("TOKEN_OPERATOR");

            switch (t[i].enums.op)
            {
            case OP_ADD:
                printf("\tOP_ADD");

                printf("\t\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case OP_ASSIGNMENT:
                printf("\tOP_ASSIGNMENT");
                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case OP_MULTIPLY:
                printf("\tOP_MULTIPLY");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;

            case OP_SUBSTRACT:
                printf("\tOP_SUBSTRACT");

                printf("\t%i\t%i\t\"%s\"\n", t[i].textPos.row, t[i].textPos.column, t[i].token_pointer);
                break;
            }
            break;
        }
    }
}