#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main() {

    float x = 0;
    float y = 0;
    float z = 0;

    printf("Введіть 'x'\n");
    scanf("%f", &x);

    printf("Введіть 'y'\n");
    scanf("%f", &y);

    printf("Введіть 'z'\n");
    scanf("%f", &z);

 

    if(x == y || x == 0 || x == -y || z == 0) {
        printf("Неможлива операція\n");
    }

    else {
        float a0 = pow(x, y + 1) / pow(x - y, 1 / z);
        float a1 = y / (4 * fabs(x + y));
        float a2 = sqrt(fabs(cos(y) / sin(x) + 2));
        float a = a0 + a1 + a2;
        
        printf("x  = ");
        printf("%.3f\n", x);
        printf("y  = ");
        printf("%.3f\n", y);
        printf("z  = ");
        printf("%.3f\n", z);
        printf("a0 = ");              
        printf("%.3f\n", a0);
        printf("a1 = ");
        printf("%.3f\n", a1);
        printf("a2 = ");
        printf("%.3f\n", a2);
        printf("a  = ");
        printf("%.3f\n", a);
    }

    return 0;
}