#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <progbase.h>
#include <progbase/canvas.h>
#include <progbase/console.h>
#include <math.h>
#include <string.h>
#include <assert.h>

struct Colors
{
    int red;
    int green;
    int blue;
};

struct Vec2D
{
    double x;
    double y;
};

struct Ball
{
    struct Vec2D loc;
    int radius;
    float rotRadius;
    float speed;
};

struct Vec2D fromPolar(float angle0, float length0);//
struct Vec2D add(struct Vec2D a, struct Vec2D b);//
float length(struct Vec2D v);//
struct Vec2D negative(struct Vec2D v);//
struct Vec2D mult(struct Vec2D v, float n);//
struct Vec2D norm(struct Vec2D v);//
struct Vec2D rotate(struct Vec2D v, float angle);//
float distance(struct Vec2D a, struct Vec2D b);//
float angle(struct Vec2D v);//
int equals(struct Vec2D a, struct Vec2D b);//

struct Ball createBall(float w, float h, int x, int x1, const float dt, int R, int rotRadMax);
struct Ball updateBall(struct Ball b, float rotAngle, int i, double *px, double *py, int x, int x1);
struct Colors color();
void drawBall(struct Ball b, struct Colors col);
int randIntNum(int max, int min);
float randFloatNum(float max, float min);

void mainTest()
{
    const float pi = 3.14159;
    float angle0 = pi;
    int length0 = 10;

    struct Vec2D V = {3, 4};
    struct Vec2D V1 = {1, 1};
    float n = 5;
    assert(length(V) == n);
    //----------------------
    V = mult(V, n);
    assert(V.x == 15 && V.y == 20);
    //----------------------
    V = negative(V);
    assert(V.x == -15 && V.y == -20);
    //----------------------
    V = add(V, V1);
    assert(V.x == -14 && V.y == -19);
    //----------------------
    assert(distance(V, V1) == 25);
    //----------------------
    struct Vec2D V3 = {10, 10};
    V3 = norm(V3);

    if (fabs((V3.x*V3.x + V3.y*V3.y) - 1) < 0.0001)
    {
        //good
    }
    else
    {
        assert(V3.x = 3/5 && V3.y == 4/5);
    }
    //----------------------
    struct Vec2D vec0 = fromPolar(angle0, length0);
    float a = angle(vec0);
    assert(a == angle0);
    //----------------------
    struct Vec2D vec1 = fromPolar(angle0, length0);
    int eq = equals(vec0, vec1);
    assert(eq == 1);
    //----------------------
    vec0 = rotate(vec0, pi);
    a = angle(vec0);
    if (a < 0.0001)
    {
        //good
    }
    else
    {
        assert(a == 0);
    }
    //----------------------
    printf("Testing completed\n");
    printf("SUCCESS\n");
}

int main(int argc, char *argv[])
{
    Console_clear();

    int numBalls = 3;

    for (int i = 0; i < argc; i++) 
    {
        if (strcmp(argv[i], "-t") == 0)
        {
            mainTest();

            return 0;
        }

        if (strcmp(argv[i], "-n") == 0)
        {
            numBalls = atoi(argv[i + 1]);
            break;
        }
    }

    Canvas_invertYOrientation();
    struct ConsoleSize consoleSize = Console_size();
    const float widthPixels = consoleSize.columns;
    const float heightPixels = consoleSize.rows * 2;
    Canvas_setSize(widthPixels, heightPixels);

    srand(time(0));

    const float pi = 3.1415;
    const float dt = 0.0002;
    float rotAngle = 0;
    int newTime;
    int x = 7;
    int x1 = 1;
    int rotRadMax = 24;
    int R = 8;

    struct Colors col[numBalls];
    struct Ball ball[numBalls];

    //creating balls
    for (int i = 0; i < numBalls; i++)
    {
        ball[i] = createBall(widthPixels, heightPixels, x, x1, dt, R, rotRadMax);
        x += 1;
        x1 += 1;

        col[i] = color();
    }

    while (!Console_isKeyDown())
    {
        newTime = time(0);
        rotAngle += pi / 2;

        //updating balls
        for (int i = 1; i < numBalls; i++)
        {
            double *px = &ball[i - 1].loc.x;
            double *py = &ball[i - 1].loc.y;
            ball[i] = updateBall(ball[i], rotAngle, i, px, py, x, x1);
        }

        Canvas_beginDraw();
        //
        for (int i = 0; i < numBalls; i++)
        {
            drawBall(ball[i], col[i]);
        }

        struct Vec2D vec = fromPolar(-rotAngle / 1300, 30);
        struct Vec2D nulVec = fromPolar(0, 0);
        Canvas_setColorRGB(255, 255, 255);
        Canvas_putPixel(widthPixels / 2 + vec.x, heightPixels / 2 + vec.y);
        //
        Canvas_setColorRGB(255, 100, 70);
        Canvas_strokeLine(widthPixels / 2, heightPixels / 2, widthPixels / 2 + vec.x, heightPixels / 2 + vec.y);
        //
        struct Vec2D vec1 = add(nulVec, vec);
        Canvas_setColorRGB(5, 100, 70);
        Canvas_strokeLine(ball[1].loc.x, ball[1].loc.y, vec1.x + widthPixels / 2, vec1.y + heightPixels / 2);
        Canvas_endDraw();
    }

    return 0;
}

//functions
struct Ball createBall(float w, float h, int x, int x1, const float dt, int R, int rotRadMax)
{
    int max = 6;
    int min = 2;
    struct Ball ball;
    ball.loc.x = w / 2;
    ball.loc.y = h / 2;
    ball.rotRadius = rotRadMax - x;
    ball.radius = R - x1;
    ball.speed = (rand() % (max - min + max) + min) * dt;
    int key = rand() % (2 - 1 + 1) + 1;

    if (key == 2)
    {
        ball.speed = -ball.speed;
    }

    return ball;
}

struct Ball updateBall(struct Ball b, float rotAngle, int i, double *px, double *py, int x, int x1)
{

    b.loc.x = (*px) + b.rotRadius * cos(rotAngle * b.speed);
    b.loc.y = (*py) + b.rotRadius * sin(rotAngle * b.speed);

    return b;
}

struct Colors color()
{
    struct Colors col;

    col.red = rand() % 256;
    col.green = rand() % 256;
    col.blue = rand() % 256;

    return col;
}

void drawBall(struct Ball b, struct Colors col)
{
    Canvas_setColorRGB(col.red, col.green, col.blue);
    Canvas_fillCircle(b.loc.x, b.loc.y, b.radius);
}

struct Vec2D fromPolar(float angle0, float length0)
{
    struct Vec2D v;
    v.x = cos(angle0) * length0;
    v.y = sin(angle0) * length0;

    return v;
}

struct Vec2D add(struct Vec2D a, struct Vec2D b)
{
    struct Vec2D v;
    v.x = a.x + b.x;
    v.y = a.y + b.y;

    return v;
}

int randIntNum(int max, int min)
{
    int num = rand() % (max - min + 1) + min;

    return num;
}

float randFloatNum(float max, float min)
{
    float scale = rand() / (float)RAND_MAX;

    return min + scale * (max - min);
}

float length(struct Vec2D v)
{
    float l = 0;

    l = sqrt(pow(v.x, 2) + pow(v.y, 2));
    return l;
}

struct Vec2D negative(struct Vec2D v)
{
    struct Vec2D newVec = v;
    newVec.x = -newVec.x;
    newVec.y = -newVec.y;

    return newVec;
}

struct Vec2D mult(struct Vec2D v, float n)
{
    struct Vec2D newVec = v;
    newVec.x *= n;
    newVec.y *= n;

    return newVec;
}

struct Vec2D norm(struct Vec2D v)
{
    float norm = sqrt(pow(v.x, 2) + pow(v.y, 2));
    v.x = v.x / norm;
    v.y = v.y / norm;
    return v;
}

struct Vec2D rotate(struct Vec2D v, float angle)
{
    struct Vec2D newVec;
    newVec.x = v.x * cos(angle) - v.y * sin(angle);
    newVec.y = v.y * sin(angle) + v.y * cos(angle);

    return newVec;
}

float distance(struct Vec2D a, struct Vec2D b)
{
    float l = 0;
    struct Vec2D newVec;
    newVec.x = a.x - b.x;
    newVec.y = a.y - b.y;
    l = length(newVec);

    return l;
}

float angle(struct Vec2D v)
{
    float an = 0;
    struct Vec2D newVec;
    newVec.x = 1;
    newVec.y = 0;

    an = acos((newVec.x * v.x + newVec.y * v.y) / (length(newVec) * length(v)));
    return an;
}

int equals(struct Vec2D a, struct Vec2D b)
{
    if (a.x == b.x && a.y == b.y)
    {
        return 1;
    }
    return 0;
}