#include <stdlib.h>
#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

enum TOKENS
{
    TOKEN_KEYWORD,
    TOKEN_OPERATOR,
    TOKEN_DELIMITER,
    TOKEN_LITERAL,
    TOKEN_IDENTIFIER,
};

enum LIT
{
    LIT_INTEGER,
    LIT_FLOAT,
    LIT_STRING,
};

enum OP
{
    OP_ASSIGNMENT,
    OP_ADD,
    OP_SUBSTRACT,
    OP_MULTIPLY,
};

enum KW
{
    KW_INTEGER,
    KW_RETURN,
};

enum DEL
{
    DEL_COMMA,
    DEL_SEMICOLON,
    DEL_LEFTPAR,
    DEL_RIGHTPAR,
    DEL_LEFTCURL,
    DEL_RIGHTCURL,
};

struct Enums
{
    enum LIT lit;
    enum OP op;
    enum KW kw;
    enum DEL del;
};

struct Table
{
    char *items;
    size_t row_capacity;
    size_t col_capacity;
    size_t count;
};

struct token
{
    size_t count_tok;
    char *token_pointer;
    enum TOKENS tokType;
    struct Enums enums;
};

void addString(struct Table *ptable, char *str);
int parsecode(char *p, struct Table *ptable, struct token *ptok, struct Table *plit, struct Table *pid, int *pNumTok, int *pNumId, int *pNumLit, int *pNumKw);
void printappopt(struct token *tok, int *pNumTok);
bool containsString(const struct Table *ptable, const char *str);
char *findString(const struct Table *ptable, const char *str);
char *getStringAt(const struct Table *ptable, int index);
char *readNum(char *dest, char *src, int length, int *pInt);
char *readPunkt(char *dest, char *src);
char *readWord(char *dest, char *src, int length, bool *isLit);
bool containString(struct Table *ptable, char *str);


int main()
{
    Console_clear();

    const int nKeywords = 50;
    const int nMaxKwLength = 50;
    const int nMaxIdentifiers = 50;
    const int nMaxIdLength = 50;
    const int nMaxLiterals = 50;
    const int nMaxLitLength = 50;

    int numTokens = 0;
    int *pNumTok = &numTokens;

    int numLit = 0;
    int *pNumLit = &numLit;

    int numId = 0;
    int *pNumId = &numId;

    int numKw = 0;
    int *pNumKw = &numKw;

    char kwords[nKeywords][nMaxKwLength];
    char id[nMaxIdentifiers][nMaxIdLength];
    char lit[nMaxLiterals][nMaxLitLength];

    struct Table keywords;
    struct Table identifiers;
    struct Table literals;
    struct token token_st[100];

    keywords.count = 0;
    keywords.row_capacity = nMaxKwLength;
    keywords.col_capacity = nKeywords;
    keywords.items = &kwords[0][0];

    identifiers.count = 0;
    identifiers.row_capacity = nMaxIdentifiers;
    identifiers.col_capacity = nMaxIdLength;
    identifiers.items = &id[0][0];

    literals.count = 0;
    literals.row_capacity = nMaxLiterals;
    literals.col_capacity = nMaxLitLength;
    literals.items = &lit[0][0];

    addString(&keywords, "int");
    addString(&keywords, "return");

    char string[] = "puts(\"\\\"Hello\\\"\\t\\n\");\nint ch = (int)4.5;\nint g = 5;\ng = -g + ch * 10;\nreturn;";
    char *p = string;
    printf(">>>\n\n");
    printf("%s\n\n", p);
    printf("<<<\n\n");

    parsecode(p, &keywords, token_st, &literals, &identifiers, pNumTok, pNumId, pNumLit, pNumKw);

    printf("\n");
    printappopt(token_st, pNumTok);
    printf("\n");

    printf("\nAmount of tokens: %d\n", *pNumTok);

    int a = 0;

    printf("\nLITERALS (%i):\n", numLit);
    while (a < numLit)
    {
        printf("%s\n", &lit[a][0]);
        a++;
    }

    a = 0;

    printf("\nIDENTIFIERS (%i):\n", numId);
    while (a < numId)
    {
        printf("%s\n", &id[a][0]);
        a++;
    }

    a = 0;

    printf("\nKEYWORDS (%i):\n", numKw);
    while (a < numKw)
    {
        printf("%s\n", &kwords[a][0]);
        a++;
    }

    return 0;
}

char *getStringAt(
    const struct Table *ptable,
    int index)
{
    char *p = (*ptable).items;
    p += index * (*ptable).row_capacity;
    return p;
}

char *findString(
    const struct Table *ptable,
    const char *str)
{
    for (int i = 0; i < (*ptable).count; i++)
    {
        char *p = getStringAt(ptable, i);
        if (strcmp(p, str) == 0)
        {
            return p;
        }
    }
    return NULL;
}

void addString(struct Table *ptable, char *str)
{
    int row_index = (*ptable).count;
    char *p = (*ptable).items;
    p += row_index * (*ptable).row_capacity;
    strcpy(p, str);
    (*ptable).count += 1;
}

char *readString(struct Table *ptable, int i)
{
    char *p = (*ptable).items;
    p += i * (*ptable).row_capacity;

    return p;
}

bool containString(struct Table *ptable, char *str)
{
    int length = (*ptable).count;
    for (int i = 0; i < length; i++)
    {
        char *p = readString(ptable, i);
        if (strcmp(p, str) == 0)
        {
            return true;
        }
    }

    return false;
}

char *readWord(char *dest, char *src, int length, bool *isLit)
{
    int counter = 0;
    if (*isLit == true)
    {
        *dest = '\\';
        dest++;
        *dest = '\"';
        dest++;

        while (isalnum(*src) || *src == '_' || *src == '"' || *src == '\\')
        {
            if (++counter >= length)
            {
                return (NULL);
            }

            *dest = *src;
            dest++;
            src++;
        }
        dest--;
    }
    else
    {
        while (isalnum(*src) || *src == '_')
        {
            if (++counter >= length)
            {
                return (NULL);
            }

            *dest = *src;
            dest++;
            src++;
        }
    }
    *dest = '\0';
    src--;
    *isLit = false;
    return src;
}

char *readPunkt(char *dest, char *src)
{
    *dest = *src;
    src++;
    dest++;
    *dest = '\0';

    return src;
}

char *readNum(char *dest, char *src, int length, int *pInt)
{
    int counter = 0;

    while (isdigit(*src))
    {
        *dest = *src;
        dest++;
        if (++counter >= length)
        {
            return (NULL);
        }

        src++;
    }

    if (*src == '.')
    {
        *pInt = 1;
        *dest = *src;
        dest++;
        src++;
        if (!isdigit(*src))
        {
            Console_clear();
            printf("ERROR: WRONG FLOAT\n");
            return (NULL);
        }
        else
        {
            while (isdigit(*src))
            {
                *dest = *src;
                dest++;
                src++;
            }
            src--;
        }
    }

    *dest = '\0';

    return src;
}

int parsecode(char *p, struct Table *ptable, struct token *ptok, struct Table *plit, struct Table *pid, int *pNumTok, int *pNumId, int *pNumLit, int *pNumKw)
{
    const int bufLen = 50;
    char buf[bufLen];
    int *pInt;
    int number = 0;
    pInt = &number;
    bool intRead = false;
    bool retRead = false;
    bool isLit = false;

    while (*p)
    {
        if (isspace(*p))
        {
            p++;
        }

        if (ispunct(*p))
        {
            readPunkt(buf, p);

            if (strcmp(buf, "\"") == 0)
            {
                p++;
                if (isalpha(*p))
                {
                    isLit = true;
                    p = readWord(buf, p, bufLen, &isLit);

                    if (containString(plit, buf) == false)
                    {
                        addString(plit, buf);
                        *pNumLit += 1;
                    }

                    ptok->token_pointer = findString(plit, buf);
                    ptok->enums.lit = LIT_STRING;
                    ptok->tokType = TOKEN_LITERAL;
                    ptok->count_tok += 1;
                    *pNumTok += 1;
                    ptok++;
                    continue;
                }
                else
                {
                    p--;
                }
            }

            if (strcmp(buf, "\"") == 0 || strcmp(buf, "\\") == 0 || strcmp(buf, " ") == 0)
            {
                p++;
                continue;
            }

            if (*p != '-' && *p != '(' && *p != ')' && *p != '}' && *p != '{')
            {
                p++;
                if (isalpha(*p))
                {
                    Console_clear();
                    printf("ERROR: WRONG CHAR\n");
                    return 1;
                }

                else
                {
                    p--;
                }
            }

            if (*p == '.')
            {
                p++;

                if (isdigit(*p))
                {
                    Console_clear();
                    printf("ERROR: WRONG FLOAT\n");
                    return 1;
                }
                else
                    p--;
            }

            if (strcmp(buf, ";") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_SEMICOLON;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, ",") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_SEMICOLON;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "{") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_LEFTCURL;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "}") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_RIGHTCURL;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "(") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_LEFTPAR;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, ")") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_DELIMITER;
                ptok->enums.del = DEL_RIGHTPAR;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "-") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_SUBSTRACT;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "+") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_ADD;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "*") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_MULTIPLY;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }

            if (strcmp(buf, "=") == 0)
            {
                if (containString(plit, buf) == false)
                {
                    addString(plit, buf);
                    *pNumLit += 1;
                }

                ptok->token_pointer = findString(plit, buf);
                ptok->tokType = TOKEN_OPERATOR;
                ptok->enums.op = OP_ASSIGNMENT;
                ptok->count_tok += 1;
                *pNumTok += 1;
                ptok++;
            }
        }

        if (isalpha(*p) || *p == '_')
        {
            p = readWord(buf, p, bufLen, &isLit);

            if (strcmp(buf, "n") == 0 || strcmp(buf, "t") == 0 || strcmp(buf, " ") == 0)
            {
                p++;
                continue;
            }

            if (containString(ptable, buf) == true)
            {
                for (int j = 0; j < ptable->count; j++)
                {

                    if (strcmp(buf, "int") == 0)
                    {
                        ptok->token_pointer = findString(ptable, buf);
                        ptok->tokType = TOKEN_KEYWORD;
                        ptok->enums.kw = KW_INTEGER;
                        ptok->count_tok += 1;
                        *pNumTok += 1;
                        if (intRead == false)
                        {
                            *pNumKw += 1;
                            intRead = true;
                        }

                        ptok++;
                        break;
                    }

                    if (strcmp(buf, "return") == 0)
                    {
                        ptok->token_pointer = findString(ptable, buf);
                        ptok->tokType = TOKEN_KEYWORD;
                        ptok->enums.kw = KW_RETURN;
                        ptok->count_tok += 1;
                        *pNumTok += 1;
                        if (retRead == false)
                        {
                            *pNumKw += 1;
                            retRead = true;
                        }
                        ptok++;
                        break;
                    }
                }
            }

            else
            {
                if (containString(plit, buf) == true)
                {
                    p++;
                    continue;
                }
                else
                {
                    if (containString(pid, buf) == false)
                    {
                        addString(pid, buf);
                        *pNumId += 1;
                    }

                    ptok->tokType = TOKEN_IDENTIFIER;
                    ptok->token_pointer = findString(pid, buf);
                    ptok->count_tok += 1;
                    *pNumTok += 1;
                    ptok += 1;
                    p++;
                    continue;
                }
            }
        }

        if (isalnum(*p))
        {
            p = readNum(buf, p, bufLen, pInt);

            if (*p == ';')
                p--;

            if (strcmp(buf, "") == 0)
            {
                p++;
                continue;
            }

            if (*pInt == 0)
            {
                ptok->enums.lit = LIT_INTEGER;
            }

            if (*pInt == 1)
            {
                ptok->enums.lit = LIT_FLOAT;
                *pInt = 0;
            }

            if (containString(plit, buf) == false)
            {
                addString(plit, buf);
                *pNumLit += 1;
            }

            ptok->tokType = TOKEN_LITERAL;
            ptok->token_pointer = findString(plit, buf);
            ptok->count_tok += 1;
            *pNumTok += 1;
            ptok += 1;
            p++;
            continue;
        }
        p++;
    }

    return 0;
}

void printappopt(struct token *t, int *pNumTok)
{
    for (int i = 0; i < 29; i++)
    {
        switch (t[i].tokType)
        {
        case TOKEN_KEYWORD:
            printf("TOKEN_KEYWORD");

            switch (t[i].enums.kw)
            {
            case KW_INTEGER:
                printf("\tKW_INTEGER");

                printf("\t\"int\"\n");
                break;

            case KW_RETURN:
                printf("\tKW_RETURN");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;
            }
            break;

        case TOKEN_LITERAL:
            printf("TOKEN_LITERAL");

            switch (t[i].enums.lit)
            {
            case LIT_FLOAT:
                printf("\tLIT_FLOAT");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case LIT_INTEGER:
                printf("\tLIT_INTEGER");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case LIT_STRING:
                printf("\tLIT_STRING");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;
            }
            break;

        case TOKEN_DELIMITER:
            printf("TOKEN_DELIMITER");

            switch (t[i].enums.del)
            {
            case DEL_COMMA:
                printf("\tDEL_COMMA");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case DEL_LEFTCURL:
                printf("\tDEL_LEFTCURL");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case DEL_LEFTPAR:
                printf("\tDEL_LEFTPAR");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case DEL_RIGHTCURL:
                printf("\tDEL_RIGHTCURL");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case DEL_RIGHTPAR:
                printf("\tDEL_RIGHTPAR");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case DEL_SEMICOLON:
                printf("\tDEL_SEMICOLON");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;
            }
            break;

        case TOKEN_IDENTIFIER:
            printf("TOKEN_IDENTIFIER");

            printf("\t\t\"%s\"\n", t[i].token_pointer);
            break;

        case TOKEN_OPERATOR:
            printf("TOKEN_OPERATOR");

            switch (t[i].enums.op)
            {
            case OP_ADD:
                printf("\tOP_ADD");

                printf("\t\t\"%s\"\n", t[i].token_pointer);
                break;

            case OP_ASSIGNMENT:
                printf("\tOP_ASSIGNMENT");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case OP_MULTIPLY:
                printf("\tOP_MULTIPLY");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;

            case OP_SUBSTRACT:
                printf("\tOP_SUBSTRACT");

                printf("\t\"%s\"\n", t[i].token_pointer);
                break;
            }
            break;
        }
    }
}