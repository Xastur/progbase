#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <progbase.h>
#include <progbase/console.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

int conReadLine(char str[], int maxBufLen)
{
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength - 1] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}

enum State
{
    NotReadingInt,
    ReadingInt,
};

int main()
{
    int keyMM = 0;
    int charMenu = 0;
    int stringMenu = 0;
    char ch;
    char userCh;
    int yPos = 1;
    int xPos = 50;
    int yPosStr = 11;
    int xPosStr = 1;
    int N = 0;
    int N1 = 0;
    char trash;
    int pos = 0;
    int len = 0;
    int length = 0;
    int randCharPosY;
    int randCharPosX;
    int mult = 0;
    char keyt = 0;
    bool num1;
    bool num3;
    bool punct1;
    bool punct3;
    bool isAllowed;
    bool isEndOfStr;
    srand(time(0));

    Console_clear();

    do
    {
        num1 = true;
        num3 = true;
        punct1 = true;
        punct3 = true;
        isAllowed = true;
        isEndOfStr = false;

        printf("1. Characters\n2. String\n3. Text\n4. Quit\n");

        scanf("%i", &keyMM);
        Console_clear();

        if (keyMM == 2)
        {
            printf("Enter N: ");
            scanf("%i", &N1);
            N = N1 + 1; //last element is /0
        }

        char arr2[N1];
        arr2[N1] = 0;
        const int bufLen = N;
        char buf[bufLen];

        Console_clear();

        while (keyMM == 1)
        {
            yPos = 1;
            xPos = 50;

            Console_setCursorPosition(1, 1);
            printf("1. Alphanumeric\n");
            printf("2. Alphabetic (lowercase)\n");
            printf("3. Alphabetic (uppercase)\n");
            printf("4. Alphabetic (all)\n");
            printf("5. Decimal digit\n");
            printf("6. Hexadecimal digit\n");
            printf("7. Punctuation\n");
            printf("0. < Back");

            printf("\n");
            scanf("%i", &charMenu);

            switch (charMenu)
            {
            case 1:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (isalnum(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 2:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (islower(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 3:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (isupper(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 4:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (isalpha(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 5:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (isdigit(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 6:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (isxdigit(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 7:
            {
                Console_clear();

                for (int i = 0; i < 127; i++)
                {
                    ch = i;

                    if (ispunct(ch))
                    {
                        Console_setCursorPosition(yPos, xPos);
                        printf("(%i) - %c", i, i);
                        xPos += 11;

                        if (xPos > 125)
                        {
                            xPos = 50;
                            yPos += 1;
                        }
                    }
                }
            }
            break;

            case 0:
            {
                Console_clear();

                keyMM = 0;
            }
            break;
            }
        }

        while (keyMM == 2)
        {
            Console_setCursorPosition(1, 1);
            printf("1. Заповнити рядок введеним значенням із консолі\n");
            printf("2. Очистити рядок\n");
            printf("3. Вивести підрядок із заданої позиції і заданої довжини\n");
            printf("4. Вивести список підрядків, розділених заданим символом\n");
            printf("5. Вивести найкоротше слово\n");
            printf("6. Знайти та вивести всі дробові числа, що містяться у рядку\n");
            printf("7. Знайти та вивести добуток всіх цілих чисел, що містяться у рядку\n");
            printf("0. < Back");

            for (int i = 0; i < 128; i++)
            {
                Console_setCursorPosition(9, i);
                printf("-");
            }

            if (num1 == true)
            {
                randCharPosX = 15;
                randCharPosY = 11;
                num1 = false;

                for (int i = 0; i <= N1; i++)
                {
                    arr2[i] = rand() % (126 - 33 + 1) + 33;

                    if (i == N1)
                    {
                        arr2[i] = '\0';
                    }
                }

                Console_setCursorPosition(11, 1);
                printf("Entered (%i): %s", N - 1, arr2);
            }

            Console_setCursorPosition(27, 1);
            scanf("%i", &stringMenu);

            switch (stringMenu)
            {
            case 1:
            {
                Console_clear();
                num1 = false;
                yPosStr = 11;
                xPosStr = 1;
                keyMM = 2;

                buf[0] = '\0';

                printf("Enter string: ");
                scanf("%c", &trash);
                int strLen = conReadLine(buf, bufLen);
                len = strLen;
                Console_clear();

                Console_setCursorPosition(yPosStr, xPosStr);
                printf("Entered (%d): \"%s\" \n", strLen, buf);
            }
            break;

            case 2:
            {
                Console_clear();
                keyMM = 2;

                if (num1 == false)
                {
                    int strLen = conReadLine(buf, bufLen);
                    buf[0] = '\0';
                    Console_setCursorPosition(11, 1);
                    printf("Entered (%d): \"%s\"\n", strLen, buf);
                    len = strLen;
                }
                if (num1 == true)
                {
                    num1 = false;
                    Console_setCursorPosition(11, 1);
                    printf("Entered (0): \"\" \n");
                }
            }
            break;

            case 3:
            {
                if (len == 0)
                {
                    Console_clear();

                    Console_setCursorPosition(17, 1);
                    printf("Empty string\n");
                }

                else
                {
                    printf("Enter pos: ");
                    scanf("%i", &pos);

                    if (pos < 0)
                    {
                        printf("Error: pos < 0\n");
                    }
                    if (pos > N)
                    {
                        printf("Error: pos > N\n");
                        break;
                    }
                    printf("Enter length: ");
                    scanf("%i", &length);
                    int A = pos;
                    char letters[N];

                    for (int i = 0; i < N; i++)
                    {
                        if (pos == A + length)
                        {
                            letters[i] = '\0';
                            break;
                        }
                        if (pos > N)
                        {
                            letters[i - 1] = '\0';
                            break;
                        }

                        letters[i] = buf[pos];
                        pos++;
                    }
                    Console_clear();

                    Console_setCursorPosition(17, 1);
                    printf("Output: %s", letters);
                }

                int strLen = len;
                Console_setCursorPosition(yPosStr, xPosStr);
                printf("Entered (%d): \"%s\" \n", strLen, buf);
            }
            break;

            case 4:
            {
                if (len == 0)
                {
                    Console_clear();

                    Console_setCursorPosition(17, 1);
                    printf("Empty string\n");
                }

                else
                {
                    Console_clear();

                    printf("Enter char: ");
                    userCh = Console_getChar();
                    printf("\n");

                    char letters[len];
                    int num = 0;
                    char x;
                    Console_clear();

                    Console_setCursorPosition(15, 1);
                    for (int i = 0; i < len; i++)
                    {
                        x = buf[i];

                        if (x != userCh)
                        {
                            letters[num] = x;
                            letters[num + 1] = '\0';
                            num += 1;
                        }
                        if (x == userCh)
                        {
                            isAllowed = false;
                            letters[num] = '\0';
                        }

                        if (isAllowed == false || (i == len - 1))
                        {
                            isAllowed = true;
                            printf("%s", letters);
                            printf("\n");
                            letters[0] = '\0';
                            num = 0;
                        }
                    }
                }

                int strLen = len;
                Console_setCursorPosition(yPosStr, xPosStr);
                printf("Entered (%d): \"%s\" \n", strLen, buf);
            }
            break;

            case 5:
            {
                if (len == 0)
                {
                    Console_clear();

                    Console_setCursorPosition(17, 1);
                    printf("Empty string\n");
                }

                else
                {
                    Console_clear();

                    char letters[len];
                    char smallStr[len];
                    char out[len];
                    char x;
                    int num = 0;
                    bool flag = false;

                    for (int i = 0; i < len; i++)
                    {
                        smallStr[i] = buf[i];
                    }
                    smallStr[len] = '\0';

                    for (int i = 0; i <= len; i++)
                    {
                        x = buf[i];
                        bool IsAlpha = isalpha(x);

                        if (IsAlpha)
                        {
                            letters[num] = x;
                            num++;
                            flag = true;
                            continue;
                        }
                        if ((!IsAlpha && num > 0) || (i == len))
                        {
                            letters[num] = '\0';

                            if (strlen(letters) < strlen(smallStr))
                            {
                                strcpy(smallStr, letters);
                                strcpy(out, smallStr);
                                num = 0;
                                letters[0] = '\0';
                                continue;
                            }

                            else
                            {
                                continue;
                            }
                        }
                    }

                    if (flag == true)
                    {
                        Console_clear();
                        Console_setCursorPosition(17, 1);
                        printf("%s", smallStr);
                    }
                    else
                    {
                        Console_clear();
                        Console_setCursorPosition(17, 1);
                        printf("No words in the string\n");
                    }
                }

                int strLen = len;
                Console_setCursorPosition(yPosStr, xPosStr);
                printf("Entered (%d): \"%s\" \n", strLen, buf);
            }
            break;

            case 6:
            {
                if (len == 0)
                {
                    Console_clear();

                    Console_setCursorPosition(17, 1);
                    printf("Empty string\n");
                }

                else
                {
                    Console_clear();

                    char buf1[100];
                    int bufX = 0;
                    enum State state = NotReadingInt;
                    int y = 16;
                    int x = 2;
                    bool dotSaved = false;
                    bool flag = false;

                    for (int i = 0;; i++)
                    {
                        Console_setCursorPosition(y, x);
                        char ch = buf[i];
                        bool digit = isdigit(ch);
                        bool other = !digit;

                        if (state == NotReadingInt)
                        {
                            if (other)
                            {
                                //nothing
                            }

                            if (ch == '.')
                            {
                                dotSaved = true;
                                buf1[bufX] = ch;
                                bufX++;
                                state = ReadingInt;

                                continue;
                            }

                            else if (digit)
                            {
                                buf1[bufX] = ch;
                                bufX += 1;
                                state = ReadingInt;

                                continue;
                            }
                        }

                        if (state == ReadingInt)
                        {
                            if (dotSaved == true && (ch == '.'))
                            {
                                buf1[bufX] = '\0';
                                float fl = atof(buf1);
                                printf("%f", fl);
                                y++;

                                bufX = 0;
                                buf1[bufX] = '\0';
                                buf1[bufX] = '.';
                                bufX++;
                                flag = true;

                                continue;
                            }

                            if (dotSaved == true && other)
                            {
                                buf1[bufX] = '\0';
                                float fl = atof(buf1);
                                Console_setCursorPosition(y, x);
                                printf("%.3f", fl);
                                y++;

                                bufX = 0;
                                buf1[0] = '\0';
                                state = NotReadingInt;
                                dotSaved = false;
                                flag = true;

                                continue;
                            }

                            if (dotSaved == false && (ch == '.'))
                            {
                                buf1[bufX] = ch;
                                bufX++;
                                dotSaved = true;
                                continue;
                            }

                            if (dotSaved == false && other)
                            {
                                bufX = 0;
                                buf1[0] = '\0';
                                state = NotReadingInt;
                                continue;
                            }

                            if (digit)
                            {
                                buf1[bufX] = ch;
                                bufX++;
                                continue;
                            }
                        }

                        if (ch == '\0')
                            break;
                    }

                    if (flag == false)
                    {
                        Console_clear();

                        Console_setCursorPosition(17, 1);
                        printf("No floats in the string\n");
                    }
                }

                int strLen = len;
                Console_setCursorPosition(yPosStr, xPosStr);
                printf("Entered (%d): \"%s\" \n", strLen, buf);
            }
            break;

            case 7:
            {
                if (len == 0)
                {
                    Console_clear();

                    Console_setCursorPosition(17, 1);
                    printf("Empty string\n");
                }

                else
                {
                    Console_clear();

                    char buf1[100];
                    int bufX = 0;
                    mult = 1;
                    enum State state = NotReadingInt;
                    bool flag = false;

                    for (int i = 0;; i++)
                    {
                        char ch = buf[i];
                        bool digit = isdigit(ch);
                        bool other = !digit;

                        if (state == NotReadingInt)
                        {
                            if (other)
                            {
                                printf("Error\n");
                                return 1;
                            }
                            else if (digit)
                            {
                                buf1[bufX] = ch;
                                bufX += 1;
                                state = ReadingInt;
                                flag = true;
                            }
                        }
                        else if (state == ReadingInt)
                        {
                            if (other)
                            {
                                buf1[bufX] = '\0';
                                int val = atoi(buf1);
                                mult = mult * val;
                                //printf("> %i\n", val);
                                //puts(buf1);
                                bufX = 0;

                                state = NotReadingInt;
                            }
                            else if (digit)
                            {
                                buf1[bufX] = ch;
                                bufX += 1;
                            }
                        }
                        if (ch == '\0')
                        {
                            break;
                        }
                    }
                    if (flag == true)
                    {
                        Console_clear();

                        Console_setCursorPosition(17, 1);
                        printf("%i", mult);
                    }
                    else
                    {
                        Console_clear();

                        Console_setCursorPosition(17, 1);
                        printf("No integers in the string\n");
                    }
                }

                int strLen = len;
                Console_setCursorPosition(yPosStr, xPosStr);
                printf("Entered (%d): \"%s\" \n", strLen, buf);
            }
            break;

            case 0:
            {
                Console_clear();
                keyMM = 0;
            }
            break;
            }
        }

        while (keyMM == 3)
        {
            Console_clear();
            //keyt = 0;

            char text[] = "I\'ve watched through his eyes, I have listened through his ears, and I tell you he is the one. Or at least as close as we are going to get. That is what you said about the brother. The brother tested out impossible. For other reasons. Nothing to do with his ability. Same with the sister. And there are doubts about him. He is too malleable. Too willing to submerge himself in someone else\'s will. Not if the other person is his enemy. So what do we do? Surround him with enemies all the time? If we have to. I thought you said you liked this kid. If the buggers get him, they will make me look like his favorite uncle. All right. We are saving the world, after all. Take him.";
            int N = 101;
            char page[N];
            char numKey = 0;

            switch (keyt)
            {
            case 0:
            {
                Console_clear();

                for (int i = 0; i < N; i++)
                {
                    if (i == N - 1)
                        page[i] = '\0';

                    else
                        page[i] = text[i];
                }

                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 1 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'd')
                {
                    keyt += 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;

            case 1:
            {
                Console_clear();

                for (int i = 0; i < N; i++)
                {
                    if (i == N - 1)
                        page[i] = '\0';

                    else
                        page[i] = text[i + (N - 1) * keyt + 1];
                }

                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 2 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'a')
                {
                    keyt -= 1;
                }
                if (numKey == 'd')
                {
                    keyt += 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;

            case 2:
            {
                Console_clear();

                for (int i = 0; i < N; i++)
                {
                    if (i == N - 1)
                        page[i] = '\0';

                    else
                        page[i] = text[i + (N - 1) * keyt + 1];
                }
                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 3 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'a')
                {
                    keyt -= 1;
                }
                if (numKey == 'd')
                {
                    keyt += 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;

            case 3:
            {
                Console_clear();

                for (int i = 0; i < N; i++)
                {
                    if (i == 100)
                        page[i] = '\0';

                    else
                        page[i] = text[i + (N - 1) * keyt + 1];
                }

                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 4 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'a')
                {
                    keyt -= 1;
                }
                if (numKey == 'd')
                {
                    keyt += 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;

            case 4:
            {
                Console_clear();

                for (int i = 0; i < N; i++)
                {
                    if (i == N - 1)
                        page[i] = '\0';

                    else
                        page[i] = text[i + (N - 1) * keyt + 1];
                }

                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 5 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'a')
                {
                    keyt -= 1;
                }
                if (numKey == 'd')
                {
                    keyt += 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;

            case 5:
            {
                Console_clear();

                for (int i = 0; i < N; i++)
                {
                    if (i == N - 1)
                        page[i] = '\0';

                    else
                        page[i] = text[i + (N - 1) * keyt + 1];
                }

                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 6 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'a')
                {
                    keyt -= 1;
                }
                if (numKey == 'd')
                {
                    keyt += 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;

            case 6:
            {
                Console_clear();

                for (int i = 0;; i++)
                {
                    if (text[i + (N - 1) * keyt + 1] == '\0')
                    {
                        page[i] = '\0';
                        break;
                    }

                    else
                        page[i] = text[i + (N - 1) * keyt + 1];
                }

                printf("%s", page);

                Console_setCursorPosition(10, 89);
                printf("page 7 of 7");

                Console_setCursorPosition(20, 1);
                numKey = Console_getChar();
                if (numKey == 'a')
                {
                    keyt -= 1;
                }
                if (numKey == '0')
                {
                    keyMM = 0;
                    Console_clear();
                }
            }
            break;
            }
        }

    } while (keyMM != 4);

    Console_reset();
    Console_clear();

    return 0;
}