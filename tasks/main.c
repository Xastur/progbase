#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#define SIZE 18

int conReadLine(char str[], int maxBufLen)
{
    fgets(str, maxBufLen, stdin);
    int bufLength = strlen(str);
    if (str[bufLength - 1] == '\n')
    {
        str[bufLength] = '\0';
        bufLength -= 1;
    }
    else
    {
        for (char ch; (ch = getchar()) != '\n';)
        {
        }
    }
    return bufLength;
}

int main()
{
    char buf[SIZE];
    const int bufLen = SIZE;
    unsigned long long int val = 0;

    int strLen = conReadLine(buf, bufLen);
    int len = strLen;

    if (len > SIZE)
    {
        return 1;
    }

    else
    {
        char buf1[len];
        int bufX = 0;

        for (int i = 0;; i++)
        {
            char ch = buf[i];
            bool Hex = isxdigit(ch);
            bool other = !isxdigit(ch);

            if (Hex)
            {
                if (ch == 'a' || ch == 'A')
                {
                    buf1[bufX] = 10;
                    bufX += 1;
                    continue;
                }
                if (ch == 'b' || ch == 'B')
                {
                    buf1[bufX] = 11;
                    bufX += 1;
                    continue;
                }
                if (ch == 'c' || ch == 'C')
                {
                    buf1[bufX] = 12;
                    bufX += 1;
                    continue;
                }
                if (ch == 'd' || ch == 'D')
                {
                    buf1[bufX] = 13;
                    bufX += 1;
                    continue;
                }
                if (ch == 'e' || ch == 'E')
                {
                    buf1[bufX] = 14;
                    bufX += 1;
                    continue;
                }
                if (ch == 'f' || ch == 'F')
                {
                    buf1[bufX] = 15;
                    bufX += 1;
                    continue;
                }
                else
                {
                    buf1[bufX] = ch - 48;
                    bufX += 1;
                    continue;
                }
            }

            if (ch == '\n')
            {
                buf1[bufX] = '\0';
                for (int j = 0; j < bufX; j++)
                {
                    val += powl(16, j) * (buf1[bufX - j - 1]);

                    if (j == bufX - 1)
                    {
                        printf("%llu\n", val);
                        return 0;
                    }
                }
            }

            if (other)
            {
                return 1;
            }
        }
    }
}